import { IMainViewController } from "../../../../framework/definitions/IMainViewController";
import { __name__(pascalCase)DS } from "../datastores/__name__(pascalCase)DS";
import { __name__(pascalCase)DI } from "../config/__name__(pascalCase)DI";

export class __name__(pascalCase)Controller implements IMainViewController {
    public datastore: __name__(pascalCase)DS;
    private useCases: __name__(pascalCase)DI;

    constructor(useCases: __name__(pascalCase)DI, dataStore: __name__(pascalCase)DS){
        this.useCases = useCases
        this.datastore = dataStore
    }

    async afterMount(params: any, history: any): Promise<void> {
        // Here you will call the use case
        // Ex. 
        // const versionNumber = await this.useCases.queries.getVersion.run()
    }

    afterRender(): void {
        // When component is rendered
    }

    afterUnmount(): void {
        // This is used if you want to finish some task when component is unmounted
    }
} 