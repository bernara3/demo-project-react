import React from 'react';
import { observer } from 'mobx-react';
import Page from '../../../components/Page';
import './__name__(pascalCase)Page.scss';
import { use__name__(pascalCase)Controller } from '../config/__name__(pascalCase)Setup';

export const __name__(pascalCase)Page = observer(() => {
  const { store } = use__name__(pascalCase)Controller()
  const dataStore = store.datastore

  return (
    <Page title="New page" controller={store}>
      new page __name__(pascalCase)
    </Page>
  );
});

