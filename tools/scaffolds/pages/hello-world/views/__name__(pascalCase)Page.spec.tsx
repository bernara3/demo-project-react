import React from "react";
import { render, fireEvent, screen } from '@testing-library/react'
import { __name__(pascalCase)Page } from "./__name__(pascalCase)Page";
import { __name__(pascalCase)Context } from "../../config/__name__(pascalCase)Setup";
import { use__name__(pascalCase)Controller } from "../../config/__name__(pascalCase)Setup";
import { __name__(pascalCase)Controller } from "../../controllers/__name__(pascalCase)Controller";
import { __name__(pascalCase)DI } from "../../config/__name__(pascalCase)DI";
import { __name__(pascalCase)DS } from "../../datastores/__name__(pascalCase)DS";
import { mocked } from 'ts-jest/utils';
import { shallow } from 'enzyme';

// jest.mock('../datastores/__name__(pascalCase)DS', () => {
//     return {
//         __name__(pascalCase)DS: jest.fn().mockImplementation(() => {
//             return {
//                 versionNumber: "1.2.0",
//                 changeVersion: jest.fn()
//             };
//         })
//     };
// });

describe("__name__(pascalCase)Page", () => {
    // // mock
    // const __name__(pascalCase)DSMock = mocked(__name__(pascalCase)DS, true);
    // beforeEach(() => {
    //     __name__(pascalCase)DSMock.mockClear();
    // });

    // test("display version number", async () => {
    //           const wrapper = shallow(<AlloyDetailPage />);

        // expect(wrapper.find(AlloyTitle)).toHaveLength(1);
    // })

    // test("click must call changeVersion", async () => {
    //     // arrange
    //     const ds = new __name__(pascalCase)DS()
    //     let changeVersion = jest.spyOn(ds, 'changeVersion').mockImplementation(() => {});
    //     const controller = new __name__(pascalCase)Controller(new __name__(pascalCase)DI(), ds);

    //     // act
    //     render(
    //         <helloWorldContext.Provider value={{ store: controller }}>
    //             <__name__(pascalCase)Page />
    //         </helloWorldContext.Provider>
    //     );
    //     fireEvent.click(screen.getByText('Upgrade button'))

    //     // assert
    //     expect(changeVersion).toHaveBeenCalledTimes(1);
    //     changeVersion.mockRestore();
    // })
})