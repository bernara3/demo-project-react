import React from "react";
import { __name__(pascalCase)DI } from "./__name__(pascalCase)DI";
import { __name__(pascalCase)DS } from "../datastores/__name__(pascalCase)DS";
import { __name__(pascalCase)Controller } from "../controllers/__name__(pascalCase)Controller";

const useCasesInjection = new __name__(pascalCase)DI();
const datastore = new __name__(pascalCase)DS();

export const controller = new __name__(pascalCase)Controller(useCasesInjection, datastore);
export const __name__(pascalCase)Context = React.createContext({ store: controller })
export const use__name__(pascalCase)Controller = () => React.useContext(__name__(pascalCase)Context);