import { Product } from './../../business/entities/Product';
import { action, computed, observable } from "mobx";
import { Cart } from '../../business/entities/Cart';

export class AppDatastore {
    
    // Product

    @observable
    products: Product[] = []

    // Cart

    @observable
    cart?: Cart

    @action
    setCart(cart: Cart) {
        this.cart = cart
    }

    @action
    resetCart() {
        this.cart = new Cart({})
    }

    @computed
    get qteInCart(): number{
        const qte = this.cart?.itemList.reduce((accum, item) => accum + item.qte, 0)

        if(!qte)
            return 0

        return qte
    }

    @computed
    get amount(){
        return this.cart?.itemList.reduce((accum, item) => accum + (item.qte * item.productPrice), 0)
    }

    @computed
    get cartFilled(){
        if(!this.cart)
            return false

        return this.cart?.itemList.length > 0
    }
};