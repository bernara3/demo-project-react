import React from "react";
import { AppDatastore } from "./AppDatastore";

export class AppDatastoreProvider {

    private static instance?: AppDatastore;

    static createInstance() {
        const store = new AppDatastore();
        return store;
    }

    static getStore() {
        if (!this.instance) {
            this.instance = this.createInstance();
        }
        return this.instance;
    }
}

export const useAppDatastore = () => React.useContext(
    React.createContext({ 
        appDatastore: AppDatastoreProvider.getStore(),
    })
);
