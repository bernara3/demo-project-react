import { Product } from '../../../business/entities/Product';
import { IProductGateway } from '../../../business/gateways/product/IProductGateway';
import { collection, getDocs } from "firebase/firestore";
import { db } from '../../../config/bootstrap/Bootstrap';
import { doc, getDoc } from "firebase/firestore";
import { NotFoundError } from '../../../framework/definitions/NotFoundError';

export class ProductApi implements IProductGateway {
    
    async fetchAll(): Promise<Product[]> {
        const querySnapshot = await getDocs(collection(db, "products"));

        return querySnapshot.docs.map((doc) => {            
            return this.mapToProduct(doc.data(), doc.id)
        });
    }

    async fetchOne(productID: string): Promise<Product> {                
        const docRef = doc(db, "products", productID);
        const docSnap = await getDoc(docRef);

        if (docSnap.exists()) {
            return this.mapToProduct(docSnap.data(), docSnap.id)
        } else {            
            throw new NotFoundError()
        }
    }

    private mapToProduct(data: any, id: string): Product{
        return new Product({...data, id: id})
    }
}