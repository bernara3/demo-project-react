import { CartItem } from './../../business/entities/CartItem';
import { Cart } from '../../business/entities/Cart';
import { ICartGateway } from './../../business/gateways/cart/ICartGateway';
export class CartStorage implements ICartGateway{
    
    fetch(): Promise<Cart> {
        return new Promise((resolve, _) => {
            const cartRaw = localStorage.getItem('cart')            

            if(!cartRaw)
                return resolve(new Cart({}))

            const itemsRaw = JSON.parse(cartRaw).items

            resolve(new Cart({...JSON.parse(cartRaw), items: itemsRaw.map((item: any) => new CartItem({...item}))}))
        })
    }

    persist(cart: Cart): Promise<void> {
        return new Promise((resolve, _) => {
            localStorage.setItem('cart', JSON.stringify(cart));
            resolve()
        })
    }

    clean(): Promise<void> {
        return new Promise((resolve, _) => {
            localStorage.setItem('cart', JSON.stringify(new Cart({})));
            resolve()
        })
    }

}