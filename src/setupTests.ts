// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect';

// setup enzyme
import Adapter from 'enzyme-adapter-react-16'
import { configure as configureEnzyme } from 'enzyme'
configureEnzyme({ adapter: new Adapter() })

/**
 * fix: `matchMedia` not present, legacy browsers require a polyfill
 */
global.matchMedia = global.matchMedia || function() {
    return {
      matches : false,
      addListener : function() {},
      removeListener: function() {}
    }
  }