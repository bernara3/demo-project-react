import React from 'react';
import './StandartList.scss';
import { useHistory } from 'react-router';
import { IonIcon, IonItem, IonLabel, IonText } from '@ionic/react';
import { List } from '../layout/List';
import { StandartItem } from './StandartItem';

export const StandartList = ({ items, emptyLabel }: { items?: StandartItem[], emptyLabel: string }) => {
  const history = useHistory()

  if (!items || items.length === 0) {
    return (
      <div>{emptyLabel}</div>
    )
  }

  return (
    <List
      items={items}
      label={(item: StandartItem) =>
        <StandartItem_ item={item} />
      }
      onItemClicked={(item: StandartItem) => {
        item.link && history.push(item.link)
      }}
    />
  );
}

export const StandartItem_ = ({ item }: { item: StandartItem }) =>
  <IonItem id={`standart-item-${item.id}`} lines="full" style={{ width: "100%" }}>
    <IonIcon icon={item.icon} slot="start" />
    <IonLabel>
      <IonText color="secondary">
        <h2>{item.title}</h2>
      </IonText>
      <IonText color="medium">
        <p>{item.subtitle}</p>
      </IonText>
    </IonLabel>
  </IonItem>
