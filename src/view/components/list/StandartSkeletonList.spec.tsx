import { shallow } from "enzyme"
import React from "react"
import { StandartSkeletonList } from "./StandartSkeletonList";

describe("StandartSkeletonList", () => {

    test("display a skeleton list", () => {
        const standartList = shallow(<StandartSkeletonList nbLines={5} />)

        expect(standartList.find("List")).toBeTruthy()
    })
})
