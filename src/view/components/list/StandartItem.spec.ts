import { StandartItem } from "./StandartItem"

describe("StandartItem", () => {

    test("has an id", () => {
        const subject = new StandartItem({ id: "1" })

        expect(subject.id).toEqual("1")
    })

    test("has a title", () => {
        const subject = new StandartItem({ title: "Line 1" })

        expect(subject.title).toEqual("Line 1")
    })

    test("can have a subtitle", () => {
        const subject = new StandartItem({ subtitle: "Line 2" })

        expect(subject.subtitle).toEqual("Line 2")
    })

    test("can have a link", () => {
        const subject = new StandartItem({ link: "/plate/1" })

        expect(subject.link).toEqual("/plate/1")
    })

    test("can have an icon", () => {
        const subject = new StandartItem({ icon: "foobar" })

        expect(subject.icon).toEqual("foobar")
    })

})
