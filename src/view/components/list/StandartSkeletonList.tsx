import React from 'react';
import './StandartList.scss';
import { IonIcon, IonItem, IonLabel, IonSkeletonText, IonText } from '@ionic/react';
import { List } from '../layout/List';
import { homeSharp } from 'ionicons/icons';

export const StandartSkeletonList = ({ nbLines }: { nbLines: number }) => {
  return (
    <List
      items={new Array(nbLines)}
      label={() => <StandartItemSkeleton />}
      onItemClicked={() => { }}
      lines="none"
    />
  );
}

export const StandartItemSkeleton = () =>
  <IonItem lines="full" button detail style={{ width: "100%" }}>
    <IonIcon icon={homeSharp} slot="start" />
    <IonLabel>
      <IonText color="secondary">
        <h2><IonSkeletonText animated /></h2>
      </IonText>
      <IonText color="medium">
        <p><IonSkeletonText /></p>
      </IonText>
    </IonLabel>
  </IonItem> 