import { mount, shallow } from "enzyme"
import React from "react"
import { Router } from "react-router";
import { StandartList } from "./StandartList"


describe("StandartList", () => {

    test("display a List with items", () => {
        const standartList = shallow(<StandartList items={[]} emptyLabel="Aucun items" />)

        expect(standartList.find("List")).toBeTruthy()

    })

    describe("when a item is clicked", () => {
        test("navigate to `item.link`", () => {
            const history = {
                ...jest.requireActual('react-router-dom'),
                push: jest.fn(), location: {}, listen: jest.fn()
            };
            const wrapper = mount(
                <Router history={history}>
                    <StandartList items={[{ id: "1", title: "2", link: "/plate/1" }]} emptyLabel="Aucun items" />
                </Router>,
            )

            const firstItem = wrapper.find('#standart-item-1').at(0);
            firstItem.simulate("click")

            expect(history.push).toBeCalled()
        })

        describe("when item do not have a link", () => {
            test("click do nothing (no navigation)", () => {
                const history = {
                    ...jest.requireActual('react-router-dom'),
                    push: jest.fn(), location: {}, listen: jest.fn()
                };
                const wrapper = mount(
                    <Router history={history}>
                        <StandartList items={[{ id: "1", title: "2" }]} emptyLabel="Aucun items" />
                    </Router>,
                )

                const firstItem = wrapper.find('#standart-item-1').at(0);
                firstItem.simulate("click")

                expect(history.push).not.toBeCalled()
            })
        })
    })

    describe("when no items", () => {

        test("display `empty label` (1 = [])", () => {
            const standartList = shallow(<StandartList items={[]} emptyLabel="Aucun items" />)

            expect(standartList.debug()).toContain("Aucun items")
        })

        test("display `empty label` (2 undefined)", () => {
            const standartList = shallow(<StandartList items={undefined} emptyLabel="Aucun items" />)

            expect(standartList.debug()).toContain("Aucun items")
        })
    })



})
