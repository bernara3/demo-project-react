export class StandartItem {
    public id!: string
    public title!: string
    public subtitle?: string
    public link?: string
    public icon?: string

    constructor(item: Partial<StandartItem>){
        Object.assign(this, item)
    }
}