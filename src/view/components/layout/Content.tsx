import React from "react"

export const Content = ({ children, props }: { children: any, [x: string]: any; }) => {
    return <div className={"content"} {...props}>
        {children}
    </div>
}