import React, { ReactChild, ReactElement } from 'react';
import { useParams, useHistory } from 'react-router';
import { Layout } from './Layout';
import { LoadingPage } from './LoadingPage';
import { ErrorPage } from './ErrorPage';
import { DummyAboutController } from '../../../framework/utils/DummyController';
import { IMainViewController } from '../../../framework/definitions/IMainViewController';
import { useController } from '../../../framework/hooks/UseController';

const Page: React.FC<{ children: any, title: string, controller?: IMainViewController, loadingPage?: ReactChild, errorPage?: any, showBurgerMenu?: boolean, optionButtons?: ReactElement  }>
  = ({ children, title, controller = new DummyAboutController(), loadingPage = <LoadingPage />, errorPage, showBurgerMenu = false, optionButtons }) => {

    // TODO: dummy dans framework

    const [loading, error] = useController(controller, useParams(), useHistory())

    if (loading)
      return (<Layout title={title} showBurgerMenu={showBurgerMenu} optionButtons={optionButtons} >
        <>{loadingPage}</>
      </Layout >)

    if (error)
      return (<Layout title={title} showBurgerMenu={true}>
        { errorPage ? React.createElement(errorPage, { error: error }) : <ErrorPage error={error} />}
      </Layout >)

    return (
      <Layout title={title} showBurgerMenu={showBurgerMenu} optionButtons={optionButtons}>
        {children}
      </Layout >
    );
  };

export default Page;
