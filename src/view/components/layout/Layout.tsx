import React, { ReactElement } from "react"
import { IonPage, IonContent, IonHeader, IonTitle, IonToolbar, IonBackButton, IonButtons, IonMenuButton, isPlatform } from "@ionic/react"
import { arrowBackOutline } from "ionicons/icons"

export const Layout: React.FC<{ title: string, showBurgerMenu?: boolean, optionButtons?: ReactElement, children: ReactElement }> = ({ title, showBurgerMenu = false, optionButtons, children }) => {    
    return <IonPage>
        <IonHeader>
            <IonToolbar>
                <IonButtons slot="start" style={{ marginLeft: isPlatform('ios') ? "16px" : "default" }}>
                    {!showBurgerMenu && <IonBackButton
                                defaultHref={"/"}
                                icon={arrowBackOutline}
                                text="" />}
                    {showBurgerMenu && <IonMenuButton auto-hide="false"></IonMenuButton>}
                </IonButtons>                
                <IonTitle>{title}</IonTitle>
            </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
                {children}
        </IonContent>
    </IonPage >
}