import React from "react";
import { shallow } from 'enzyme';
import { List } from "./List";

describe("List", () => {

    test("display the items", () => {
        const wrapper = shallow(<List items={[{ id: 1, name: "foo" }, { id: 2, name: "bar" }]} label={(item) => item.name} onItemClicked={(item) => console.log(item)} />);

        expect(wrapper.contains("foo")).toBeTruthy()
        expect(wrapper.contains("bar")).toBeTruthy()
    })

    test("on click on item, return it", () => {
        const clickCallback = jest.fn()
        const wrapper = shallow(<List items={[{ id: 1, name: "foo" }, { id: 2, name: "bar" }]} label={(item) => item.name} onItemClicked={clickCallback} />);
        const itemFoo = wrapper.findWhere((node) => node.text() === "foo").at(0)

        itemFoo.simulate("click")

        expect(clickCallback).toHaveBeenCalledWith({ id: 1, name: "foo" })
    })
})