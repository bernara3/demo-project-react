import React from 'react';
import { IonList, IonItem } from '@ionic/react';

export const List:
    React.FC<{ items: any[], label: Function, onItemClicked: Function, [x:string]: any; }>
    = ({ items, label, onItemClicked, ...rest }) => {
        return (
            <IonList {...rest} style={{ background: "transparent" }}>
                {
                    items?.map((item: any, key: number) => {
                        return (
                            <IonItem key={key} onClick={() => onItemClicked(item)} style={{ cursor: "pointer" }}>
                                {label(item)}
                            </IonItem>
                        )
                    })
                }
            </IonList>
        )
    }