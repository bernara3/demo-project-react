import { IonIcon } from "@ionic/react";
import { bugSharp } from "ionicons/icons";
import React from "react"
import labels from "../../../config/locales/labels.json"

export const ErrorPage: React.FC<{ error: any }> = ({ error }) => {
    console.error({
        error: error
    });

    return <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
        <h1 style={{ marginBottom: 0 }}><IonIcon icon={bugSharp} size="large" /></h1>
        <h1 style={{ marginTop: 0 }}>{labels.ERROR_TITLE["en"]}</h1>
        <h2>{labels.SOMETHING_WENT_WRONG["en"]}</h2>
    </div>
}