import { IonTabButton, IonIcon, IonLabel, IonBadge } from "@ionic/react"
import { bookSharp, cartSharp } from "ionicons/icons"
import { observer } from "mobx-react"
import React from "react"
import { useAppDatastore } from "../../data/appDatastore/AppDatastoreProvider"

export const CartTab = observer(() => {
  const {appDatastore} = useAppDatastore()

  return <>
          <IonIcon icon={cartSharp} />
          <IonLabel>Panier</IonLabel>
          {appDatastore.qteInCart > 0 && <IonBadge color="danger">{appDatastore.qteInCart}</IonBadge> }
        </>
})

export const Tabs = observer(() => {
    const {appDatastore} = useAppDatastore()

    return  (<>
        <IonTabButton tab="products" href="/products">
          <IonIcon icon={bookSharp} />
          <IonLabel>Produits</IonLabel>
        </IonTabButton>

        <IonTabButton tab="cart" href="/cart">
          <IonIcon icon={cartSharp} />
          <IonLabel>Panier</IonLabel>
          {appDatastore.qteInCart > 0 && <IonBadge color="danger">{appDatastore.qteInCart}</IonBadge> }
        </IonTabButton>
      </>)

})