import { Product } from '../../../../business/entities/Product';

export class ProductViewModel {

    readonly id: string
    readonly title: string
    readonly description: string
    readonly price: string
    readonly image: string

    constructor(product: Partial<Product>){
        this.id = product.id!
        this.title = product.name!
        this.description = product.description!
        this.image = product.image!
        this.price = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'CHF' }).format(product.price!)
    }   
}