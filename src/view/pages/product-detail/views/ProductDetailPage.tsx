import React from 'react';
import { observer } from 'mobx-react';
import Page from '../../../components/layout/Page';
import { IonButton, useIonToast } from '@ionic/react';
import { useProductDetail } from '../config/ProductDetailSetup';
import "./ProductDetailPage.scss"

export const ProductDetailPage = observer(() => {
  const { datastore, controller } = useProductDetail()  
  const [present] = useIonToast();
  
  const handleClickCart = () => {
    present('Ajouté au panier', 400)
    controller.clickAddToCart()
  }

  // TODO: VIEW MODEL POUR LE PRIX
  return (
    <Page title={"Produit"} controller={controller}>
      <div className="product-detail-cart">
        <img src={datastore.product?.image} />
        <h1>{datastore.product?.title}</h1>
        <p>{datastore.product?.description}</p>
        <h2>{datastore.product?.price}</h2>
        <IonButton onClick={handleClickCart}>Ajouter au panier</IonButton>
      </div>
    </Page>
  );
});