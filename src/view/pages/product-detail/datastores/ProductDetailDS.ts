import { Product } from '../../../../business/entities/Product';
import { action, observable } from "mobx"
import { ProductViewModel } from '../viewModel/ProductViewModel';

export class ProductDetailDS {
    @observable
    product?: ProductViewModel
    product_?: Product // keep in memory

    @action
    setProduct(product: Product) {
        this.product_ = product
        this.product = new ProductViewModel({...product})
    }
}