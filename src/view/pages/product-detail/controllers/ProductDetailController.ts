import { ProductDetailDS } from '../datastores/ProductDetailDS';
import { ProductDetailDI } from '../config/ProductDetailDI';
import { IMainViewController } from '../../../../framework/definitions/IMainViewController';

export class ProductDetailController implements IMainViewController {
    
    private datastore: ProductDetailDS;
    private useCases: ProductDetailDI;

    constructor(useCases: ProductDetailDI, dataStore: ProductDetailDS){
        this.useCases = useCases
        this.datastore = dataStore
    }

    async afterMount(params: any, history: any): Promise<void> {
        const product = await this.useCases.queries.getProduct.run(params.ID)
        
        this.datastore.setProduct(product)
    }


    afterRender(): void {
        // When component is rendered        
    }

    afterUnmount(): void {        
        // This is used if you want to finish some task when component is unmounted
    }

    clickAddToCart(): void {
        const product = this.datastore.product_!
        this.useCases.commands.addProductInCart.run(product)
    }
} 