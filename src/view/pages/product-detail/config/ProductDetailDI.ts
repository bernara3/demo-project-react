import { addProductInCart } from '../../../../config/di/business/UseCaseInjection';
import { getProduct } from '../../../../config/di/business/UseCaseInjection';

export class ProductDetailDI {

    public queries = {
        getProduct: getProduct()
    }

    public commands = {
        addProductInCart: addProductInCart()
    }

}