import { ProductDetailDS } from '../datastores/ProductDetailDS';
import React from "react";
import { ProductDetailController } from '../controllers/ProductDetailController';
import { ProductDetailDI } from './ProductDetailDI';

const useCasesInjection = new ProductDetailDI();
const datastore = new ProductDetailDS();

export const controller = new ProductDetailController(useCasesInjection, datastore);
export const context = React.createContext({ controller: controller, datastore: datastore })
export const useProductDetail = () => React.useContext(context);