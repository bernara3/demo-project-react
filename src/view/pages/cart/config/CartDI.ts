import { cleanCart, changeQteInCart } from './../../../../config/di/business/UseCaseInjection';
import { getCart } from "../../../../config/di/business/UseCaseInjection"

export class CartDI {

    public queries = {
        getCart: getCart()
    }

    public commands = {
        cleanCart: cleanCart(),
        changeQteInCart: changeQteInCart(),
    }
}