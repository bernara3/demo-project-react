import { CartDS } from './../datastores/CartDS';
import { CartController } from './../controllers/CartController';
import { CartDI } from './CartDI';
import React from "react";

const useCasesInjection = new CartDI();
const datastore = new CartDS();

export const controller = new CartController(useCasesInjection, datastore);
export const context = React.createContext({ controller: controller, datastore: datastore })
export const useCartController = () => React.useContext(context);