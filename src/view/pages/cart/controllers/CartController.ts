import { CartDS } from './../datastores/CartDS';
import { CartDI } from '../config/CartDI';
import { Cart } from '../../../../business/entities/Cart';
import { Product } from '../../../../business/entities/Product';
import { IMainViewController } from '../../../../framework/definitions/IMainViewController';

export class CartController implements IMainViewController {

    public datastore: CartDS;
    private useCases: CartDI;

    constructor(useCases: CartDI, dataStore: CartDS){
        this.useCases = useCases
        this.datastore = dataStore
    }

    async afterMount(params: any, history: any): Promise<void> {}

    afterRender(): void {
        // When component is rendered
    }

    afterUnmount(): void {
        // This is used if you want to finish some task when component is unmounted
    }

    async clickCleanCart() {
        await this.useCases.commands.cleanCart.run()
    }

    async clickIncreaseQte(product: Product) {
        await this.useCases.commands.changeQteInCart.run({ productID: product.id })
    }

    async clickDecreaseQte(product: Product) {
        await this.useCases.commands.changeQteInCart.run({ productID: product.id, decrease: true })
    }
} 