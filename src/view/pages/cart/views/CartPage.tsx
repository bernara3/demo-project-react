import React from 'react';
import { observer } from 'mobx-react';
import Page from '../../../components/layout/Page';
import { useCartController } from '../config/CartSetup';
import "./CartPage.scss"
import { CartTitle } from './connected/CartTitle';
import { Amount } from './connected/Amount';
import { ProductList } from './connected/ProductList';
import { OrderButton } from './connected/OrderButton';
import { CleanCartButton } from './connected/CleanCartButton';

export const CartPage = observer(() => {
  const { controller } = useCartController()

  return (
    <Page title={"Panier"} controller={controller} showBurgerMenu={true}>
      <div id={"cart-page"}>
        <div id="main-block">
          <CartTitle />
          <ProductList />
        </div>
        <div id="buttons-block">
          <Amount />
          <OrderButton />
          <CleanCartButton />
        </div>
      </div>
    </Page>
  );
});