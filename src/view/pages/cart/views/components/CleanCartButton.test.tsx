import { mount, shallow } from "enzyme"
import { spy } from "mobx"
import React from "react"
import { CartTitle } from "./CartTitle"
import { CleanCartButton } from "./CleanCartButton"

describe("CleanCartButton", () => {

    test("show clean button", () => {
        const wrapper = shallow(<CleanCartButton qteInCart={1} onClickCleanCart={() => {}}  />)

        expect(wrapper.text()).toContain("Tout effacer")
    })

    describe("when click", () => {
        test("call onClickCleanCart", () => {
            const onClickCleanCart = jest.fn()
            const wrapper = mount(<CleanCartButton qteInCart={1} onClickCleanCart={onClickCleanCart} />)
                        
            wrapper.find('IonButton').simulate("click")
            
            expect(onClickCleanCart).toBeCalled()
        })
    })
})