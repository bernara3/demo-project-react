import { IonButton } from "@ionic/react";
import React from "react";
import "./CartTitle.scss"

export const CartTitle = ({ qteInCart } : { qteInCart: number }) => {

    const title = <h1 id="cart-title">Panier</h1>

    if(qteInCart === 0)
        return <div id="cart-block">
            {title}
            <h2>Vous n'avez pas encore de produit dans votre panier</h2>
            <img src={"./assets/shop-illustatration.png"} />
            <IonButton routerLink="/tabs/products" routerDirection="root">Continuer vos achats</IonButton>
        </div>

    return <>
        {title}
        <p id="cart-summary">{qteInCart} produit{qteInCart > 1 && "s"} dans votre panier</p>
    </>
}
