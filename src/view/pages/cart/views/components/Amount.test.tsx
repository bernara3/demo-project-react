import { shallow } from "enzyme"
import React from "react"
import { Amount } from "./Amount"

describe("Amount", () => {

    describe("when amount is 0", () => {
        test("show nothing", () => {
            const wrapper = shallow(<Amount amount={0} />)

            expect(wrapper.text()).toEqual("")
        })
    })

    describe("when amount greater than 0", () => {
        test("show amount", () => {
            const wrapper = shallow(<Amount amount={100} />)

            expect(wrapper.text()).toContain("100")
        })
    })


})