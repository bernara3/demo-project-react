import { IonButton, IonIcon } from "@ionic/react"
import { cartOutline } from "ionicons/icons"
import React from "react"

export const OrderButton = ({ cartFilled } : { cartFilled: boolean } ) => {

    if(!cartFilled)
        return <></>

    return <IonButton routerLink="/order">
        <IonIcon slot="start" icon={cartOutline} /> Commander
    </IonButton>
}