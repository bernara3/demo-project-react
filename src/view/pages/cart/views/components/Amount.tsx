import React from "react"

export const Amount = ({ amount } : { amount?: number }) => {

    if(!amount || amount == 0)
        return <></>

    return <h3>Total : {amount} CHF</h3>
}