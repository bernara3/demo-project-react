import { mount, shallow } from "enzyme"
import React from "react"
import { CartItem } from "../../../../../business/entities/CartItem"
import { Product } from "../../../../../business/entities/Product"
import { ProductCard } from "./ProductCard"

describe("ProductCard", () => {

    const item = new CartItem({ product: new Product({ name: "Hypérion", description: "Foo", price: 10, image: "./assets/hyperion.png" }) })

    test("show product (title + price)", () => {
        const wrapper = shallow(<ProductCard item={item} onIncreaseQte={() => {}} onDecreaseQte={() => {}} />)

        expect(wrapper.text()).toContain("Hypérion")
        expect(wrapper.text()).toContain("10")
    })

    describe("when click onIncreaseQte", () => {
        test("call `onIncreaseQte`", () => {
            const onIncreaseQte = jest.fn()
            const wrapper = mount(<ProductCard item={item} onIncreaseQte={onIncreaseQte} onDecreaseQte={() => {}} />)

            wrapper.find('IonButton').at(1).simulate("click")

            expect(onIncreaseQte).toBeCalled()
        })
    })

    describe("when click onDecreaseQte", () => {
        test("call `onDecreaseQte`", () => {
            const onDecreaseQte = jest.fn()
            const wrapper = mount(<ProductCard item={item} onIncreaseQte={() => {}} onDecreaseQte={onDecreaseQte} />)

            wrapper.find('IonButton').at(0).simulate("click")

            expect(onDecreaseQte).toBeCalled()
        });
    })
    
})