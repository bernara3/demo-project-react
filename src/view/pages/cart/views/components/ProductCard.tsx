import React from 'react';
import { IonButton, IonCard, IonCardContent } from '@ionic/react';
import { CartItem } from '../../../../../business/entities/CartItem';
import "./ProductCard.scss"

export const ProductCard = ({ item, onDecreaseQte, onIncreaseQte } : { item: CartItem, onDecreaseQte: Function, onIncreaseQte: Function }) => {

    return(<>
        <IonCard className="product-card">
          <IonCardContent className="product-card-content">
            <div className="product-image-block">
              <img className="product-image" src={item.productImage}></img>
            </div>
            <div className="product-detail-block">
              <div className="product-title">
                <span>{item.productName} - </span>
                <span>{item.productPrice}</span>
              </div>
              <div className="product-qte">
                <IonButton id="decrease-qte" size="small" onClick={() => onDecreaseQte(item.product)}>-</IonButton>
                {item.qte}
                <IonButton id="increase-qte" size="small" onClick={() => onIncreaseQte(item.product)}>+</IonButton>
              </div>
            </div>
          </IonCardContent>
      </IonCard>
      </>)
}