import { IonButton, IonIcon } from "@ionic/react";
import { removeCircleOutline } from "ionicons/icons";
import React from "react";

export const CleanCartButton = ({ qteInCart, onClickCleanCart } : { qteInCart: number, onClickCleanCart: Function }) => {

  if (qteInCart === 0){
    return (<></>);
  }
    
  return (
    <IonButton color={"medium"} onClick={() => onClickCleanCart()}>
      <IonIcon slot="start" icon={removeCircleOutline}></IonIcon> Tout effacer
    </IonButton>
  );
};
