import { shallow } from "enzyme"
import React from "react"
import { CartTitle } from "./CartTitle"

describe("CartTitle", () => {

    describe("when qteInCart is 0", () => {
        test("show `no products in cart`", () => {
            const wrapper = shallow(<CartTitle qteInCart={0}  />)

            expect(wrapper.text()).toContain("Vous n'avez pas encore de produit dans votre panier")
        })
    })

    describe("when qteInCart greater than 0", () => {
        test("show `1 product`", () => {
            const wrapper = shallow(<CartTitle qteInCart={1} />)

            expect(wrapper.text()).toContain("1 produit")
        })

        test("show `2 products`", () => {
            const wrapper = shallow(<CartTitle qteInCart={2} />)

            expect(wrapper.text()).toContain("2 produits")
        })
    })

})