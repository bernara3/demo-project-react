import { observer } from "mobx-react"
import React from "react"
import { useAppDatastore } from "../../../../../data/appDatastore/AppDatastoreProvider"
import { Amount as Amount_ } from "../components/Amount";

export const Amount = observer(() => {
    const { appDatastore } = useAppDatastore()

    return <Amount_ amount={appDatastore.amount} />
})