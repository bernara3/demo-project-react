import { observer } from "mobx-react";
import React from "react";
import { useAppDatastore } from "../../../../../data/appDatastore/AppDatastoreProvider";
import { useCartController } from "../../config/CartSetup";
import { CleanCartButton as _CleanCartButton } from "../components/CleanCartButton"

export const CleanCartButton = observer(() => {
  const { appDatastore } = useAppDatastore();
  const { controller } = useCartController()

  return <_CleanCartButton qteInCart={appDatastore.qteInCart} onClickCleanCart={() => controller.clickCleanCart()}  />
});
