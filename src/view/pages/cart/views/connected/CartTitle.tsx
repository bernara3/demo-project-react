import { IonButton } from "@ionic/react";
import { observer } from "mobx-react";
import React from "react";
import { useAppDatastore } from "../../../../../data/appDatastore/AppDatastoreProvider";
import { CartTitle as _CartTitle } from "../components/CartTitle"

export const CartTitle = observer(() => {
    const { appDatastore } = useAppDatastore()

    return <_CartTitle qteInCart={appDatastore.qteInCart} />
})
