import { observer } from "mobx-react"
import React from "react"
import { useAppDatastore } from "../../../../../data/appDatastore/AppDatastoreProvider"
import { useCartController } from "../../config/CartSetup"
import { ProductCard } from "../components/ProductCard"

export const ProductList = observer(() => {
    const { appDatastore } = useAppDatastore()
    const { controller } = useCartController()
    const items = appDatastore.cart?.itemList

    if(!items)
        return <></>

    return <>
        {items.map((item) => <ProductCard 
                                item={item} 
                                onIncreaseQte={() => controller.clickIncreaseQte(item.product)} 
                                onDecreaseQte={() => controller.clickDecreaseQte(item.product)} />)}
    </>
})