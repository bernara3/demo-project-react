import { observer } from "mobx-react"
import React from "react"
import { useAppDatastore } from "../../../../../data/appDatastore/AppDatastoreProvider"
import { OrderButton as _OrderButton } from "../components/OrderButton"

export const OrderButton = observer(() => {
    const { appDatastore } = useAppDatastore()

    return <_OrderButton cartFilled={appDatastore.cartFilled} />
})