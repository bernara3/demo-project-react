import React from 'react';
import Page from '../../../components/layout/Page';
import { usePlaceOrderController } from '../config/PlaceOrderSetup';
import { DoneButton } from './components/DoneButton';
import { PreparePayment } from './components/PreparePayment';
import { MailSent } from './components/MailSent';
import { OrderPlaced } from './components/OrderPlaced';
import { OrderTitle } from './components/OrderTitle';
import "./PlaceOrderPage.scss"

export const PlaceOrderPage = () => {
  const { controller } = usePlaceOrderController()
  
  // TODO: VIEW MODEL POUR LE PRIX
  return (
    <Page title={"Commande"} controller={controller}>
      <div id="order-page">
        <OrderTitle />
        <div id="check-list">
          <PreparePayment />
          <MailSent />
          <OrderPlaced />
        </div>
          <DoneButton />
      </div>
    </Page>
  );
};