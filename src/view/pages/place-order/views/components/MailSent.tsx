import { IonItem, IonIcon, IonText } from "@ionic/react";
import { checkmarkDone } from "ionicons/icons";
import { observer } from "mobx-react";
import React from "react"
import { usePlaceOrderController } from "../../config/PlaceOrderSetup";

export const MailSent = observer(() => {
    const { datastore } = usePlaceOrderController()
    
    if(!datastore.mailSent){
        return <></>
    }

    return <IonItem>
            <IonIcon slot="start" icon={checkmarkDone} color="success"></IonIcon>
            <IonText color="black">Email envoyé</IonText>
    </IonItem>
})