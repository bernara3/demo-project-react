import { IonSpinner } from "@ionic/react"
import { observer } from "mobx-react"
import React from "react"
import { usePlaceOrderController } from "../../config/PlaceOrderSetup"

export const OrderTitle = observer(() => {
    const { datastore } = usePlaceOrderController()

    if(datastore.orderPlaced){
        return <div id="order-title">
            <h1>Commande terminé</h1>
        </div> 
    }

    return <div id="order-title">
        <h1>Commande en cours...</h1>
        <IonSpinner />
    </div>
})