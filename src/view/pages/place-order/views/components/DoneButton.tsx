import { IonButton, IonIcon } from "@ionic/react";
import { checkmarkDone } from "ionicons/icons";
import { observer } from "mobx-react";
import React from "react"
import { useHistory } from "react-router";
import { usePlaceOrderController } from "../../config/PlaceOrderSetup";

export const DoneButton = observer(() => {
    const { datastore } = usePlaceOrderController()
    const history = useHistory()

    if(!datastore.orderPlaced){
        return <></>
    }

    return <div id="order-done-button-block">
        <IonButton onClick={() => history.replace("/tabs/products")}>
            <IonIcon slot="start" icon={checkmarkDone}></IonIcon>
            Terminer la commande
        </IonButton>
    </div>
    
})