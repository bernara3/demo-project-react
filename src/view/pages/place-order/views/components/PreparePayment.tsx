import { IonIcon, IonItem, IonText } from "@ionic/react";
import { checkmarkDone } from "ionicons/icons";
import { observer } from "mobx-react";
import React from "react"
import { usePlaceOrderController } from "../../config/PlaceOrderSetup";

export const PreparePayment = observer(() => {
    const { datastore } = usePlaceOrderController()
    
    if(!datastore.preparePayment){
        return <></>
    }

    return <IonItem>
        <IonIcon slot="start" icon={checkmarkDone} color="success"></IonIcon>
        <IonText color="black">Paiement réussi</IonText>
    </IonItem>
})