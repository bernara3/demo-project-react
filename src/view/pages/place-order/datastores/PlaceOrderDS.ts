import { action, observable } from "mobx"

export class PlaceOrderDS {

    @observable
    preparePayment: boolean = false

    @observable
    mailSent: boolean = false

    @observable
    orderPlaced: boolean = false

    @action
    setInventoryChecked() {
        this.preparePayment = true
    }

    @action
    setMailSent(): void {
        this.mailSent = true 
    }

    @action
    setOrderPlaced(): void {
       this.orderPlaced = true 
    }
    
    @action
    reset() {
        this.preparePayment = false 
        this.mailSent = false 
        this.orderPlaced = false 
    }

}