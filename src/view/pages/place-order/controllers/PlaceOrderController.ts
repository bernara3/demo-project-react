import { IMainViewController } from '../../../../framework/definitions/IMainViewController';
import { PlaceOrderDI } from '../config/PlaceOrderDI';
import { PlaceOrderDS } from '../datastores/PlaceOrderDS';

export class PlaceOrderController implements IMainViewController {

    public datastore: PlaceOrderDS;
    private useCases: PlaceOrderDI;

    constructor(useCases: PlaceOrderDI, dataStore: PlaceOrderDS){
        this.useCases = useCases
        this.datastore = dataStore
    }

    async afterMount(params: any, history: any): Promise<void> {
        this.useCases.commands.placeOrder.run({
            callback: {
                onInventoryChecked: () => this.datastore.setInventoryChecked(),
                onMailSent: () => this.datastore.setMailSent(),
                onOrderPlaced: () => {
                    this.datastore.setOrderPlaced()
                    this.useCases.commands.cleanCart.run()
                }
            }
        })
    }

    afterRender(): void {
        // When component is rendered
    }

    afterUnmount(): void {
        this.datastore.reset()                
    }
    
} 