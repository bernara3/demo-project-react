import { PlaceOrderDS } from '../datastores/PlaceOrderDS';
import { PlaceOrderController } from '../controllers/PlaceOrderController';
import { PlaceOrderDI } from './PlaceOrderDI';
import React from "react";

const useCasesInjection = new PlaceOrderDI();
const datastore = new PlaceOrderDS();

export const controller = new PlaceOrderController(useCasesInjection, datastore);
export const context = React.createContext({ controller: controller, datastore: datastore })
export const usePlaceOrderController = () => React.useContext(context);