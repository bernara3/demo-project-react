import { cleanCart } from './../../../../config/di/business/UseCaseInjection';
import { placeOrder } from '../../../../config/di/business/UseCaseInjection';

export class PlaceOrderDI {

    public queries = {
    }

    public commands = {
        placeOrder: placeOrder(),
        cleanCart: cleanCart()
    }

}