import { observable, action } from "mobx"
import { Product } from '../../../../business/entities/Product';
import { ProductViewModel } from "../viewModel/ProductViewModel";

export class ProductListDS {

    @observable
    productList: ProductViewModel[] = []

    @action
    setProductList(productList: Product[]) {
        this.productList = productList.map((product: Product) => new ProductViewModel({...product}))
    }
}