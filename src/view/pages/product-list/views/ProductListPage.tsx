import React from 'react';
import { observer } from 'mobx-react';
import Page from '../../../components/layout/Page';
import './ProductListPage.scss';
import { useFavoriteListController } from '../config/ProductListSetup';
import { List } from '../../../components/layout/List';
import { useHistory } from 'react-router';
import { ProductCard } from './components/ProductCard';
import { ProductViewModel } from '../viewModel/ProductViewModel';
import { useIonToast } from '@ionic/react';

export const ProductListPage = observer(() => {
  const { datastore, controller } = useFavoriteListController()
  const history = useHistory()
  const [present] = useIonToast();

  const productCart = (item: ProductViewModel) => 
    <ProductCard 
      item={item} 
      onProductClick={() => history.push(`/product-detail/${item.id}`)} 
      onAddToCartClick={() => {
        present('Ajouté au panier', 400)
        controller.clickAddToCart(item.id)
      }} />

  return (
    <Page title={"Produits"} controller={controller} showBurgerMenu={true}>
      <div id="product-list-page">
        <List 
          items={datastore.productList} 
          lines="none"
          label={(item: ProductViewModel) => productCart(item)} 
          onItemClicked={(item: any) => {
            // do nothing on card click, we delegate this in ProductCart
          }} />   
      </div>  
    </Page>
  );
});