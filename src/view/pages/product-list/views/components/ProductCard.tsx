import React from 'react';
import { IonButton, IonCard, IonCardContent } from '@ionic/react';
import "./ProductCart.scss"
import { ProductViewModel } from '../../viewModel/ProductViewModel';

export const ProductCard = ({ item, onProductClick, onAddToCartClick } : { item: ProductViewModel, onProductClick: Function, onAddToCartClick: Function }) => {
    return(<>
        <IonCard className="product-card2">
          <IonCardContent className="product-card-content">
            <div className="product-image-block">
              <img className="product-image" src={item.image}></img>
            </div>
            <div className="product-detail-block" onClick={() => onProductClick()}>
                <span className="product-title">{item.name}</span>
                <span className={`product-price`} style={item.goodPrice ? {"color": "red" } : {}} >{item.priceLabel}</span>
            </div>
            <div className="product-add-to-cart">
                <IonButton onClick={() => onAddToCartClick()}>+</IonButton>
            </div>
          </IonCardContent>
      </IonCard>
      </>)
}