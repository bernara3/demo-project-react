import { IMainViewController } from '../../../../framework/definitions/IMainViewController';
import { ProductListDS } from '../datastores/ProductListDS';
import { ProductListDI } from './../config/ProductListDI';

export class ProductListController implements IMainViewController {
    public datastore: ProductListDS;
    private useCases: ProductListDI;

    constructor(useCases: ProductListDI, dataStore: ProductListDS){
        this.useCases = useCases
        this.datastore = dataStore
    }

    async afterMount(params: any, history: any): Promise<void> {
        const productList = await this.useCases.queries.getProductList.run()

        this.datastore.setProductList(productList)
    }

    afterRender(): void {
        // When component is rendered
    }

    afterUnmount(): void {
        // This is used if you want to finish some task when component is unmounted
    }

    clickAddToCart(productID: string) {
        const product = this.datastore.productList.find(p => p.id === productID)!
        this.useCases.commands.addProductInCart.run(product)
    }
} 