import { ProductListDS } from './../datastores/ProductListDS';
import { ProductListDI } from './ProductListDI';
import React from "react";
import { ProductListController } from '../controllers/ProductListController';

const useCasesInjection = new ProductListDI();
const datastore = new ProductListDS();

export const controller = new ProductListController(useCasesInjection, datastore);
export const favoriteListContext = React.createContext({ controller: controller, datastore: datastore })
export const useFavoriteListController = () => React.useContext(favoriteListContext);