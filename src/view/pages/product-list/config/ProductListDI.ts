import { addProductInCart } from './../../../../config/di/business/UseCaseInjection';
import { getProductList } from "../../../../config/di/business/UseCaseInjection"

export class ProductListDI {

    public queries = {
        getProductList: getProductList()
    }

    public commands = {
        addProductInCart: addProductInCart()
    }

}