import { Product } from '../../../../business/entities/Product';

export class ProductViewModel extends Product {

    readonly goodPrice: boolean = false
    readonly link!: string
    readonly priceLabel!: string

    constructor(product: Partial<Product>){
        super({
            ...product
        })

        const price = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'CHF' }).format(product.price!)
        const goodPrice = product.price! <= 10 

        this.id = product.id!
        this.priceLabel = goodPrice ? `seulement ${price}` : `${price}`
        this.goodPrice = goodPrice
        this.link = `/product-detail/${product.id}`
    }
}