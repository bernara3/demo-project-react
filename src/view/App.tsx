import React from "react";
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/global.css";
import "./theme/variables.css";


import { Bootstrap } from "../config/bootstrap/Bootstrap";
import { bookSharp } from "ionicons/icons";
import { Route, Redirect } from "react-router";
import { ProductListPage } from "./pages/product-list/views/ProductListPage";
import { CartPage } from "./pages/cart/views/CartPage";
import { PlaceOrderPage } from "./pages/place-order/views/PlaceOrderPage";
import { ProductDetailPage } from "./pages/product-detail/views/ProductDetailPage";
import { CartTab } from "./components/Tabs";

// bootstrap
Bootstrap();

const App: React.FC = () => {
  return (
    <IonApp>
      <IonReactRouter>
      <IonTabs>
          <IonRouterOutlet>
            <Route path="/product-detail/:ID" component={ProductDetailPage} />
            <Route path="/order" component={PlaceOrderPage} />
            <Route path="/tabs/products" component={ProductListPage} />
            <Route path="/tabs/cart" component={CartPage} />      
            <Route exact path="/" render={() => <Redirect to="/tabs/products" />} />
          </IonRouterOutlet>

          <IonTabBar slot="bottom">
            <IonTabButton tab="products" href="/tabs/products">
              <IonIcon icon={bookSharp} />
              <IonLabel>Produits</IonLabel>
            </IonTabButton>
            <IonTabButton tab="cart" href="/tabs/cart">
              <CartTab/>
            </IonTabButton>
          </IonTabBar>
      </IonTabs>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
