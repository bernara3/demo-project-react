import { Entity } from "../definitions/Entity"
import { IMapper } from "../definitions/IMapper"

export const mapAndValidate = async (mapper: IMapper, rawEntity: object): Promise<Entity> => {
    const alloy = mapper.toEntity(rawEntity)
    await alloy.validate()
    return alloy
}

export const mapAndValidateList = async (mapper: IMapper, rawEntityList: object[]): Promise<Entity[]> => {
    const alloyList = mapper.toEntityList(rawEntityList)
    await Promise.all(alloyList.map(async (a) => await a.validate()))
    return alloyList
}