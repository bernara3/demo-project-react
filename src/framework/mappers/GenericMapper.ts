import { plainToClassFromExist } from 'class-transformer';
import { Entity } from '../definitions/Entity';
import { Newable } from '../definitions/Newable';

export class GenericMapper {
    static toEntity(rawEntity: unknown, entityClass: Newable<object>): Entity {
        return plainToClassFromExist(new entityClass({}), rawEntity) as Entity;
    }
}