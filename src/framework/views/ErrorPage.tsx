import React from "react"

export const ErrorPage: React.FC<{ error: any }> = ({ error }) => {
    console.error({
        error: error
    });

    return <div>
        <h1>Error</h1>
        <h2>Something went wrong !</h2>
    </div>
}
