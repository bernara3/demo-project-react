import { IonSpinner } from "@ionic/react"
import React from "react"

export const LoadingPage = () => {
    return <div><IonSpinner/></div>
}