import React, { ReactChild } from 'react';
import { useParams, useHistory } from 'react-router';
import { IMainViewController } from '../definitions/IMainViewController';
import { useController } from '../hooks/UseController';
import { ErrorPage } from './ErrorPage';
import { LoadingPage } from './LoadingPage';

export const Page: React.FC<{ children: any, controller: IMainViewController, loadingPage?: ReactChild, errorPage?: any }>
  = ({ children, controller, loadingPage = <LoadingPage />, errorPage }) => {

    const [loading, error] = useController(controller, useParams(), useHistory())

    if (loading)
      return (loadingPage)

    if (error) {
      const _errorPage = errorPage ? React.createElement(errorPage, { error: error }) : <ErrorPage error={error} />

      return (_errorPage)
    }

    return (children);
  };
