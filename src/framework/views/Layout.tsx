import React, { ReactElement } from "react"
import { IonPage, IonHeader, IonToolbar, IonTitle, IonContent, isPlatform, IonBackButton, IonButtons } from "@ionic/react"
import { arrowBackOutline } from "ionicons/icons"

export const Layout: React.FC<{ title: string, showBackButton?: boolean, children: ReactElement }>
  = ({ title, showBackButton = false, children }) => {
    return <IonPage>
      {!isPlatform('desktop') &&
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start" style={{ marginLeft: isPlatform('ios') ? "16px" : "default" }}>
              <IonBackButton defaultHref={showBackButton ? "/" : undefined} icon={arrowBackOutline} text="" />
            </IonButtons>
            <IonTitle slot="start">
              {title}
            </IonTitle>
          </IonToolbar>
        </IonHeader>
      }

      <IonContent fullscreen>
        <div id="page-content">
          {children}
        </div>
      </IonContent>
    </IonPage>
  }
