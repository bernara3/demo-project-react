import '@capacitor-community/http';

import { Plugins } from '@capacitor/core';
import { isPlatform } from '@ionic/react';

import axios from 'axios';

// TODO: cleanest way : strategy pattern => one for native (ios,android) and one for web (other)
export class HttpClient {

    async get(url: string, params: any = {}, headers: any = {}) {
        if (isPlatform('ios') || isPlatform('android')) {
            const { Http } = Plugins;

            const res = await Http.request({
                method: 'GET',
                url: url,
                headers: headers,
                params: params
            });
            return res.data
        }
        else {
            const res = await axios.get(url)
            return res.data
        }
    };

    async post(url: string, data: any, headers: any = { "Content-Type": "application/json" }) {
        if (isPlatform('ios') || isPlatform('android')) {
            const { Http } = Plugins;

            const res = await Http.request({
                method: 'POST',
                url: url,
                headers: headers,
                data: data
            });
            return res.data
        }
        else {
            const res = await axios.post(url, data)
            return res.data
        }
    };

}