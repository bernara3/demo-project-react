abstract class HttpError {}

export class UnauthorizedError extends HttpError {}