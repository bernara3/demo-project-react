import { validateOrReject } from 'class-validator';

export function testMandatoryField({entity, fieldName }: { entity: any, fieldName: string})  {
    it(`field "${fieldName}" is mandatory`, async () => {

        try {
            await validateOrReject(entity)
            throw new Error("Test failed : no error thrown")
        } catch (error) {
            if(error.message === "Test failed : no error thrown"){
                throw error
            }
            
            expect(error[0].property).toEqual(fieldName)
            expect(error[0].constraints).toEqual({ isDefined: `${fieldName} should not be null or undefined` })
        }
    });
}