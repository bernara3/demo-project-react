import { ValidationError } from 'class-validator';

export function testValidation({entity, constraint }: { entity: any, constraint: string})  {
    it(`${constraint}`, async () => {

        try {
            await entity.validate()
            throw new Error('Validation not working (not throwing error)')
        } catch (error) {                
            if(error instanceof Array && error[0] instanceof ValidationError) {
                expect(JSON.stringify(error[0].constraints)).toContain(constraint)
            }
            else {
                throw error;
            }
        }
    });
}