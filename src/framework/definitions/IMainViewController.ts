export type IMainViewController = {
    afterMount(params: any, history: any) : void;
    afterRender(params: any, history: any) : void;
    afterUnmount(): void;
}