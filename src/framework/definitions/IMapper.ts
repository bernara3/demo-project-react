import { Entity } from "./Entity";

export interface IMapper {
    fromEntity(entity: Entity): object
    toEntity(rawEntity: object): Entity
    fromEntityList(entity: Entity[]): object[]
    toEntityList(rawEntityList: object[]): Entity[]
} 