import { validateOrReject } from 'class-validator';
import { GenerateRandomID } from '../utils/GenerateRandomID';
export class Entity {

    public id: string = GenerateRandomID()

    async validate(): Promise<void> {
        await validateOrReject(this)
    }
}
