import { IMapper } from "./IMapper";

export type IRequester = {
    mapper: IMapper
}