export interface IParams {}
export interface IReturn {}
export interface IUseCase{
    run(params: IParams) : IReturn
}