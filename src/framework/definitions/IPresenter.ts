export interface IPresenter {
    toView(param: object): object
    fromView(param: object): object
} 