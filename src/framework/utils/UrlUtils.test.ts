import { isLowerCase } from "./StringUtils"
import { isValidUrl } from "./UrlUtils"

describe("UrlUtils", () => {
    describe("isValidUrl", () => {
        test("true if url is well formed", () => {
            expect(isValidUrl("http://test")).toEqual(true)
            expect(isValidUrl("http://test.com")).toEqual(true)
            expect(isValidUrl("http://test.com/test")).toEqual(true)

            expect(isValidUrl("abcd")).toEqual(false)
            expect(isValidUrl("")).toEqual(false)
            expect(isValidUrl("0")).toEqual(false)
            expect(isValidUrl(undefined)).toEqual(false)
            expect(isValidUrl(null)).toEqual(false)
        })
    })
})