export const ImageUrlToFile = (imageUrl: string, imageName: string): Promise<File> => {
  return new Promise((resolve, _) => {
    const request = new XMLHttpRequest();
    request.open('GET', imageUrl, true);
    request.responseType = 'blob';
    request.onloadend = function () {
      if (request.status === 404)
        throw new Error('IMAGE_404_NOT_FOUND');
    }
    request.onload = async () => {
      resolve(new File([request.response], imageName));
    }
    request.send();
  });
}

export const convertImageUrlToBase64 = (url: string, callback: any, outputFormat: string) => {
  var canvas = window.document.createElement('CANVAS') as any;
  var ctx = canvas.getContext('2d');
  var img = new Image();
  img.crossOrigin = 'Anonymous';
  img.onload = function () {
    canvas.height = img.height;
    canvas.width = img.width;
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL(outputFormat || 'image/png');
    callback.call(this, dataURL);
    // Clean up
    canvas = null;
  };
  img.src = url;
}

export const toBase64 = (file: any) => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => resolve(reader.result);
  reader.onerror = error => reject(error);
});

export const ImageBase64ToFile = (image: string): File => {
  const i = image.indexOf('base64,');
  const buffer = Buffer.from(image.slice(i + 7), 'base64');
  const imageFile = new File([buffer], "img");

  return imageFile;
}
