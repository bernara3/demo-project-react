export class ArrayUtil{

    static updateItem(id: string | number, item: any, items: any[]) {
        const objIndex = items.findIndex(obj => obj.id === id);

        return [
            ...items.slice(0, objIndex),
            item,
            ...items.slice(objIndex + 1),
        ];
    }
}
