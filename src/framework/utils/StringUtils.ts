export const isLowerCase = (str: string) => {
    return str === str.toLowerCase();
}