import { IMainViewController } from "../definitions/IMainViewController";

export class DummyAboutController implements IMainViewController {
    afterMount(params: any, history: any): void {
    }
    afterRender(params: any, history: any): void {
    }
    afterUnmount(): void {
    }
}