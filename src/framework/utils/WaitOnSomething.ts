export const WaitOnSomething = (somethingToWait: Function, callbackReady: Function, timeout: number = 250) => {
    const waitToBeDefined = () => {
        if (somethingToWait()) {
            callbackReady()
        }
        else {
            setTimeout(waitToBeDefined, timeout);
        }
    }
    waitToBeDefined()
}