import { isLowerCase } from "./StringUtils"

describe("StringUtils", () => {
    describe("isLowerCase", () => {
        test("true if is lowercase", () => {
            expect(isLowerCase("abcd")).toEqual(true)
            expect(isLowerCase("Abcd")).toEqual(false)
            expect(isLowerCase("ABCD")).toEqual(false)
        })
    })
})