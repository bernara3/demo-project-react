/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from "react";
import { IMainViewController } from "../definitions/IMainViewController";

// TODO: unit test
export const useController = (controller: IMainViewController, initialLoadParam: any, history?: any): [any, any] => {
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(undefined)

    useEffect(() => {
        const initStore = async () => {
            try {
                await controller.afterMount(initialLoadParam, history);
            }
            catch (e) {
                setError(e)
            }
            finally {
                setLoading(false);
            }
        }
        initStore();

        return () => {
            controller.afterUnmount();
        }
    }, [])

    useEffect(() => {
        controller.afterRender(initialLoadParam, history);
    })

    return [loading, error]
}