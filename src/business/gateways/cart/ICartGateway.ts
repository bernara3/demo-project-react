import { Cart } from "../../entities/Cart";

export interface ICartGateway {
    fetch(): Promise<Cart>
    persist(card: Cart): Promise<void>
    clean(): Promise<void>;
}