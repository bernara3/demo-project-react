import { Product } from "../../entities/Product";

export interface IProductGateway {
    fetchOne(productID: string): Promise<Product>
    fetchAll(): Promise<Product[]>
}