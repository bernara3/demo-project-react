import { Product } from "./Product"
import { Cart } from "./Cart"
import { CartItem } from "./CartItem"

describe("Cart", () => {

    const product1 = new Product({ id: "1", name: "Harry Potter", price: 12.50 })
    const product2 = new Product({ id: "2", name: "Hypérion", price: 9.50 })

    describe("#add", () => {
        test("add a Product in list", () => {
            const cart = new Cart({})

            cart.add(product1)
            cart.add(product2)

            expect(cart.itemList).toEqual([
                new CartItem({ product: product1, qte: 1, id: expect.anything()  }),
                new CartItem({ product: product2, qte: 1, id: expect.anything() })
            ])
        })

        describe("when adding same Product", () => {
            test("increase the qte", () => {

                const cart = new Cart({})

                cart.add(product1)
                cart.add(product1)                

                expect(cart.itemList).toEqual([
                    new CartItem({ product: product1, qte: 2, id: expect.anything() }),
                ])
            })
        })
    })

    describe("#remove", () => {
        test("remove item from cart", () => {

            const cart = new Cart({})

            cart.add(product1)
            cart.add(product2)                
            cart.remove(product1.id)                            

            expect(cart.itemList).toEqual([
                new CartItem({ product: product2, qte: 1, id: expect.anything() }),
            ])
        })

        describe("when product to delete have more than 1 qte", () => {
            test("remove all from this product (use decrease instead)", () => {

                const cart = new Cart({})

                cart.add(product1)
                cart.add(product1)
                cart.add(product2)                
                cart.remove(product1.id)                            
    
                expect(cart.itemList).toEqual([
                    new CartItem({ product: product2, qte: 1, id: expect.anything() }),
                ])

            })
        })
    })

    describe("#increaseQte", () => {
        test("increase qte of product", () => {

            const cart = new Cart({})

            cart.add(product1)
            cart.increaseQte(product1.id)

            expect(cart.itemList).toEqual([
                new CartItem({ product: product1, qte: 2, id: expect.anything() }),
            ])

        })
    })

    describe("#decreaseQte", () => {
        test("decrease qte of product", () => {

            const cart = new Cart({})

            cart.add(product1)
            cart.add(product1)
            cart.decreaseQte(product1.id)

            expect(cart.itemList).toEqual([
                new CartItem({ product: product1, qte: 1, id: expect.anything() }),
            ])

        })

        describe("when decreasing product with qte of 1", () => {
            test("remove the product", () => {
                const cart = new Cart({})

                cart.add(product1)
                cart.decreaseQte(product1.id)

                expect(cart.itemList).toEqual([])
            })
        })

    })

    describe("#productExist", () => {
        test("return true when exist", () => {
            const cart = new Cart({})

            cart.add(product1)
        
            expect(cart.productExist(product1.id)).toEqual(true)
        })
    })
   
})