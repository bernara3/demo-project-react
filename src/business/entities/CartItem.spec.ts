import { Product } from "./Product"
import { Cart } from "./Cart"
import { CartItem } from "./CartItem"

describe("CartItem", () => {

    const product = new Product({ id: "1", name: "Harry Potter", price: 12.50 })
    const item = new CartItem({ product: product, qte: 2 })

    test("#productTitle", () => {
        expect(item.productName).toEqual("Harry Potter")
    })

    test("#productPrice", () => {
        expect(item.productPrice).toEqual(12.50)
    })

})