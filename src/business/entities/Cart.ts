import { CartItem } from './CartItem';
import { Product } from "./Product";

export class Cart{
    private items: CartItem[] = []

    constructor(fields: Partial<Cart>) {
        Object.assign(this, fields);
    }

    add(product: Product){
        if(this.productExist(product.id)){
            this.items = this.items.map(item => { 
                return item.product.id === product.id ? new CartItem({ product: product, qte: item.qte + 1 }) : item
            })
            return
        }

        this.items = [...this.items, new CartItem({ product: product, qte: 1 })]
    }

    remove(productID: string){
        this.items = [...this.items.filter(item => item.product.id !== productID)]
    }

    increaseQte(productID: string){
        this.items = this.items.map(item => { 
            return item.product.id === productID ? new CartItem({ ...item, qte: item.qte + 1 }) : item
        })
    }

    decreaseQte(productID: string){
        this.items = this.items.map(item => { 
            return item.product.id === productID ? new CartItem({ ...item, qte: item.qte - 1 }) : item
        })

        // ensure to have no product with qte 0
        this.items = [...this.items.filter(item => item.qte !== 0)]
    }

    productExist(productID: string){
        return this.items.some(item => item.product.id === productID)
    }

    get itemList(){
        return this.items
    }
    
}
