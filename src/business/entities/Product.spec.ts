import { ValidationError } from "class-validator"
import { testValidation } from "../../framework/tests/testValidation"
import { testMandatoryField } from "../../framework/tests/testMandatoryField"
import { Product } from "./Product"

describe("Product", () => {

    const defaultParams = {
        title: "Hyperion",
        description: "Foo  bar",
        image: "./assets/img.png",
        price: 12.50,
    }

    describe("#name", () => {
        test("has a `name`", () => {
            const product = new Product({ ...defaultParams, name: "Hyperion" })
            expect(product.name).toEqual("Hyperion")
        })
    
        testMandatoryField({
            entity: new Product({ ...defaultParams, name: undefined }),
            fieldName: "name"
        })
    })

    describe("#price", () => {
        test("has a `price`", () => {
            const product = new Product({ ...defaultParams, price: 12.50 })
            expect(product.price).toEqual(12.50)
        })

        testValidation({
            entity: new Product({ ...defaultParams, price: 0.04 }),
            constraint: "price must not be less than 0.05"
        })
    })
    
})

