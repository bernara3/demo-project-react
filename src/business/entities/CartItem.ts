import { Entity } from "../../framework/definitions/Entity";
import { Product } from "./Product";

export class CartItem extends Entity {
    readonly product!: Product
    readonly qte!: number

    constructor(fields: Partial<CartItem>) {
        super();
        Object.assign(this, fields);
    }

    get productName(){
        return this.product.name
    }

    get productPrice(){
        return this.product.price
    }

    get productImage(){
        return this.product.image
    }
}