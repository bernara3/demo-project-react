import { IsDefined, Min } from "class-validator";
import { Entity } from "../../framework/definitions/Entity";

export class Product extends Entity{

    @IsDefined()
    readonly name!: string

    @IsDefined()
    readonly description!: string

    @IsDefined()
    readonly image!: string

    @Min(0.05)
    readonly price!: number

    constructor(fields: Partial<Product>) {
        super();
        Object.assign(this, fields);
    }
}