import { ICartGateway } from '../../gateways/cart/ICartGateway';
import { IUseCase } from "../../../framework/definitions/IUseCase";
import { Product } from '../../entities/Product';
import { AppDatastore } from '../../../data/appDatastore/AppDatastore';

export class AddProductInCart implements IUseCase {
    private gateway: ICartGateway;
    private appDatastore: AppDatastore;

    constructor(gateway: ICartGateway, appDatastore: AppDatastore){
        this.gateway = gateway
        this.appDatastore = appDatastore
    }

    async run(product: Product): Promise<void> {
        const cart = await this.gateway.fetch()
        
        cart.add(product)
        this.appDatastore.setCart(cart)
        await this.gateway.persist(cart)
    }
}