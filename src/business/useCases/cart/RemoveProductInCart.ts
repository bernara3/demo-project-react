import { ICartGateway } from '../../gateways/cart/ICartGateway';
import { IUseCase } from "../../../framework/definitions/IUseCase";
import { AppDatastore } from '../../../data/appDatastore/AppDatastore';

export class RemoveProductInCart implements IUseCase {
    private gateway: ICartGateway;
    private appDatastore: AppDatastore;

    constructor(gateway: ICartGateway, appDatastore: AppDatastore){
        this.gateway = gateway
        this.appDatastore = appDatastore
    }

    async run(productID: string): Promise<void> {
        const cart = await this.gateway.fetch()
        cart.remove(productID)
        this.appDatastore.setCart(cart)
        await this.gateway.persist(cart)
    }
}