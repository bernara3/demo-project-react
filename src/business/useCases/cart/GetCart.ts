import { ICartGateway } from '../../gateways/cart/ICartGateway';
import { IUseCase } from "../../../framework/definitions/IUseCase";
import { Cart } from '../../entities/Cart';

export class GetCart implements IUseCase {
    private gateway: ICartGateway;

    constructor(gateway: ICartGateway){
        this.gateway = gateway
    }

    async run(): Promise<Cart> {
        return await this.gateway.fetch()
    }
}