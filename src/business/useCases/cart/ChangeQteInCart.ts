import { AppDatastore } from '../../../data/appDatastore/AppDatastore';
import { IUseCase } from '../../../framework/definitions/IUseCase';
import { Cart } from '../../entities/Cart';
import { ICartGateway } from '../../gateways/cart/ICartGateway';

export type ChangeQteInCartParams =  {
    productID: string,
    decrease?: boolean
}

// TODO: test d'integration ici ? avec le localstorage

export class ChangeQteInCart implements IUseCase{

    private gateway: ICartGateway;
    private appDatastore: AppDatastore;

    constructor(gateway: ICartGateway, appDatastore: AppDatastore){
        this.gateway = gateway
        this.appDatastore = appDatastore
    }

    async run(params: ChangeQteInCartParams): Promise<Cart> {
        const { productID, decrease } = params

        const cart = await this.gateway.fetch()
        decrease ? cart.decreaseQte(productID) : cart.increaseQte(productID)

        this.appDatastore.setCart(cart)

        await this.gateway.persist(cart)

        return cart
    }
}