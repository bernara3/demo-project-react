import { ICartGateway } from '../../gateways/cart/ICartGateway';
import { IUseCase } from "../../../framework/definitions/IUseCase";
import { AppDatastore } from '../../../data/appDatastore/AppDatastore';
import { Cart } from '../../entities/Cart';

export class CleanCart implements IUseCase {
    private gateway: ICartGateway;
    private appDatastore: AppDatastore;

    constructor(gateway: ICartGateway, appDatastore: AppDatastore){
        this.gateway = gateway
        this.appDatastore = appDatastore
    }

    async run(): Promise<void> {
        this.appDatastore.setCart(new Cart({}))
        await this.gateway.clean()
        console.log("[Cart]: Cleaned.");
    }
}