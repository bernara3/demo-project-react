import { IUseCase } from "../../../framework/definitions/IUseCase";

export type PlaceOrderParams = {
    callback: {
        onInventoryChecked: () => void,
        onMailSent: () => void,
        onOrderPlaced: () => void,
    }
}

export class PlaceOrder implements IUseCase{

    run(params: PlaceOrderParams): Promise<void> {
        return new Promise(async (resolve, _)=>{

            await new Promise(resolve => setTimeout(resolve, 3000)); // 3 sec
            params.callback.onInventoryChecked()

            await new Promise(resolve => setTimeout(resolve, 3000)); // 3 sec
            params.callback.onMailSent()

            await new Promise(resolve => setTimeout(resolve, 3000)); // 3 sec
            params.callback.onOrderPlaced()

            resolve()
        })



        // 1. Place order => gateway
        // 2. catch error => throw OrderFailed

        // 3. send email
        // 4. catch error => console.error => mais continuer la transaction.

        // 5. CleanCart si c'est ok (etape 1., 3 osef)



    }


}