import { IProductGateway } from './../../gateways/product/IProductGateway';
import { IUseCase } from "../../../framework/definitions/IUseCase";
import { Product } from "../../entities/Product";

export class GetProduct implements IUseCase{

    private productGateway: IProductGateway

    constructor(productGateway: IProductGateway){
        this.productGateway = productGateway
    }

    async run(productID: string): Promise<Product> {        
        return await this.productGateway.fetchOne(productID)
    }
}