import { IUseCase } from "../../../framework/definitions/IUseCase";
import { Product } from "../../entities/Product";
import { IProductGateway } from "../../gateways/product/IProductGateway";

export class GetProductList implements IUseCase{
    private productGateway: IProductGateway

    constructor(productGateway: IProductGateway){
        this.productGateway = productGateway
    }

    async run(): Promise<Product[]> {
        return await this.productGateway.fetchAll()
    }
}