import { ChangeQteInCart } from './../../../business/useCases/cart/ChangeQteInCart';
import { CleanCart } from './../../../business/useCases/cart/CleanCart';
import { AddProductInCart } from '../../../business/useCases/cart/AddProductInCart';
import { appDatastore, cartStorage, productApi } from './../data/GatewaysInjection';
import { GetProductList } from '../../../business/useCases/product/GetProductList';
import { GetCart } from '../../../business/useCases/cart/GetCart';
import { RemoveProductInCart } from '../../../business/useCases/cart/RemoveProductInCart';
import { GetProduct } from '../../../business/useCases/product/GetProduct';
import { PlaceOrder } from '../../../business/useCases/order/PlaceOrder';
import { IUseCase } from '../../../framework/definitions/IUseCase';

// ------ Product ------ //

export const getProductList = (): GetProductList => {
    return intercept(new GetProductList(productApi));
}

export const getProduct = (): GetProduct => {
    return intercept(new GetProduct(productApi));
}

// ------ Cart ------ //

export const getCart = (): GetCart => {
    return intercept(new GetCart(cartStorage));
}

export const addProductInCart = (): AddProductInCart => {
    return intercept(new AddProductInCart(cartStorage, appDatastore));
}

export const removeProductInCart = (): RemoveProductInCart => {
    return intercept(new RemoveProductInCart(cartStorage, appDatastore));
}

export const changeQteInCart = (): ChangeQteInCart => {
    return intercept(new ChangeQteInCart(cartStorage, appDatastore));
}

export const cleanCart = (): CleanCart => {
    return intercept(new CleanCart(cartStorage, appDatastore));
}

// ------ Order ------ //

export const placeOrder = (): PlaceOrder => {
    return intercept(new PlaceOrder());
}
 
// ------ OTHER ------ //

function intercept<T extends IUseCase>(useCase: T): T {
    const proxied = new Proxy(useCase, {
        get: (target, prop) => {
            if (prop === "run") {
                console.debug({ useCase });
            }
            return Reflect.get(target, prop);
        }
    });

    return proxied
}