import { ProductApi } from './../../../data/firebase/products/ProductApi';
import { CartStorage } from './../../../data/localstorage/CartStorage';
import { AppDatastoreProvider } from "../../../data/appDatastore/AppDatastoreProvider"

// Datastore
export const appDatastore = AppDatastoreProvider.getStore()

// Cart
export const cartStorage = new CartStorage()

// Product
export const productApi = new ProductApi()