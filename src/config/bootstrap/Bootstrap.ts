import { getCart } from './../di/business/UseCaseInjection';
import { appDatastore } from '../di/data/GatewaysInjection';
import { initializeApp } from "firebase/app"
import { getFirestore } from "firebase/firestore"

export const Bootstrap = async () => {

    // initialize Cart
    const cart = await getCart().run()
    appDatastore.setCart(cart)
}

// Initialize Cloud Firestore through Firebase
initializeApp({
    apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
    authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID
});

export const db = getFirestore();