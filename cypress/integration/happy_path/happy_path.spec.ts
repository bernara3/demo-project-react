/// <reference types="Cypress" />

const product = {
    id: "RekqHdmwETnEHopzkrZu",
    name: "Pizza margherita",
    price: "14,5"
}

// TODO: MOCK FIREBASE ?
describe('Happy Path', () => {

    it('Navigate to product list', () => {
        cy.visit('http://localhost:8100/tabs/products')
    })

    it('Display title', () => {
        cy.contains(product.name)
    })

    it('Display price', () => {
        cy.contains(product.price)
    })

    it('Click on "+"', () => {
        cy.contains("+").click()
    })

    it('assert added in cart', () => {
        cy.get("#tab-button-cart").click()
        cy.contains("1 produit dans votre panier")
    })

    it('Click display new page', () => {
        cy.get("#tab-button-products").click()
        cy.contains(product.name).click()

        cy.url().should('include', '/product-detail')
    })

    it('Display add to shop button', () => {
        cy.visit(`http://localhost:8100/product-detail/${product.id}`)
        cy.contains("Ajouter au panier").click()
        cy.contains("Ajouter au panier").click()
        cy.get("#tab-button-cart").click()
        cy.contains("2 produits dans votre panier")
    })

    /*describe("when click on add to shop button", () => {

        it('display badge on tab', () => {
            // 2x
            cy.contains("Ajouter au panier").click()
            cy.contains("Ajouter au panier").click()
            // back button
            cy.get('.buttons-first-slot > .md').click()
            // 2 in tab
            cy.get("#tab-button-cart > ion-badge").contains(2)
        })
    })*/

    describe("when click on cart tab", () => {

        it('display badge on tab', () => {
            cy.get("#tab-button-cart").click()
           
            cy.url().should('include', '/cart')
        })

        it("display `2 products on cart`", () =>  {
            cy.contains("2 produits dans votre panier")
        })

        it("display product", () =>  {
            cy.contains(product.name)
        })

        it("display total", () =>  {
            cy.contains(product.price)
        })

        /*describe("when return on product list", () => {

            it("go on an other product", () =>  {
                cy.get("#tab-button-products").click()

                cy.contains("Harry").click()
                cy.url().should('include', '/product-detail')
                cy.get('.product-detail-cart > h1').contains("Harry")

                // back button
                cy.get('.buttons-first-slot > .md').click()
            })
        })*/

        describe("when click on order", () => {

            it("display order page", () =>  {
                cy.get("#tab-button-cart").click()
                cy.contains("Commander").click()
                cy.url().should('include', '/order')
                cy.wait(8000)
                cy.contains("Commande terminé")
                cy.contains("Terminer la commande").click()
                cy.url().should('include', '/products')
            })
        })
    })

})